db.inventory.insertMany([
    {
        "name": "JavaScript for Beginners",
        "author": "James Doe",
        "price": 5000,
        "stocks": 50,
        "publisher": "JS Publishing House"
    },
    {
        "name": "HTML and CSS",
        "author": "John Thomas",
        "price": 2500,
        "stocks": 38,
        "publisher": "NY Publishers"
    },
    {
        "name": "Web Development Fundamentals",
        "author": "Noah Jimenez",
        "price": 3000,
        "stocks": 10,
        "publisher": "Big Idea Publishing"
    },
    {
        "name": "Java Programming",
        "author": "David Michael",
        "price": 10000,
        "stocks": 100,
        "publisher": "JS Publishing House"
    }
]);


// Comparison Query Operators
/*  

    $gt (greater than) / $gte (greater than or equal) operator

    $gt SYNTAX: 
        db.collectionName.find(
            {
                "field": {
                    $gt: value
                }
            }
        );
    
    $gte SYNTAX: 
        db.collectionName.find(
            {
                "field": {
                    $gte: value
                }
            }
        );
    
*/

// Find the stocks that greater than 50
db.inventory.find(
    {
        "stocks": {
            $gt: 50
        }
    }
);

// Find the stocks that greater than or equal 50
db.inventory.find(
    {
        "stocks": {
            $gte: 50
        }
    }
);


/*

    $lt (less than) / $lte (less than or equal) operator
    
    $lt SYNTAX:

        db.collectionName.find(
            {
                "field": {
                    $lt: value
                }
            }
        );
    
    $lte SYNTAX:
        db.collectionName.find(
            {
                "field": {
                    $lte: value
                }
            }
        );
*/

// Find the stocks that less than 50
db.inventory.find(
    {
        "stocks": {
            $lt: 50
        }
    }
);

// Find the stocks that less than or equal 50
db.inventory.find(
    {
        "stocks": {
            $lte: 50
        }
    }
);



/*

    $ne (not equal) operator
        ► Allows us to find documents that have fields values that is not equal to a specified value

    Syntax:
        db.collectionName.find(
            {
                field: {
                    $ne: value
                }
            }
        ); 

*/

db.inventory.find(
    {
        "stocks": {
            $ne: 50
        }
    }
);


/*
    $eq (equal) operator
        ► Allows us to find documents that have fields values that is equal to a specified value

    SYNTAX:
            db.collectionName.find(
                {
                    field: {
                        $eq: value
                    }
                }
            );

*/

db.inventory.find(
    {
        "stocks": {
            $eq: 50
        }
    }
);



/*
    $in operator
        ► Allows us to find documents with specific match criteria of one field using different value

    SYNTAX:
        db.collectionName.find(
            {
                field: {
                    $in: [value1, value2]
                }
            }
        )

*/


db.inventory.find(
    {
        "price": {
            $in: [1000, 5000]
        }
    }
);


/*

    $nin (not in) operator
        ► Allows us to find documents that is NOT match for a specific criteria of one field using different value

    SYNTAX:
        db.collectionName.find(
            {
                field: {
                    $nin: [value1, value2]
                }
            }
        );

*/

db.inventory.find(
    {
        "price": {
            $nin: [1000, 5000]
        }
    }
);


// Logical Query Operators

/*
    
    $or operator
        ► 
        
        SYNTAX:
            db.collectionName.find(
                {
                    $or: [
                        {fieldA: valueA},
                        {fieldB: valueB}
                    ]
                }
            );

*/


db.inventory.find(
        {
            $or: [
                {"publisher": "JS Publishing House"},
                {"name": "HTML and CSS"}
            ]
        }
    );


// Combining Logical Operator & Comparison Operator
db.inventory.find(
        {
            $or: [
                {"author": "James Doe"},
                
                {
                    "price": {
                        $lte: 3000
                    }
                }
            ]
        }
    );


/*
    $and operator

        SYNTAX:
            db.collectionNmae.find( 
                { 
                    $and: [ 
                        {fieldA: valueA},
                        {fieldA: valueA} 
                        ]
                }
            );
*/

db.inventory.find(
        {
            $and: [
                {
                    "stocks": {
                        $ne: 50
                    }
                },
                
                {
                    "price": {
                        $ne: 5000
                    }
                }
            ]
        }
    );

// by Default:  the "$and" operator is SIMILAR to "find" Method
db.inventory.find({
    "stocks": {
        $ne:50
    },
    "price": {
        $ne: 5000
    }
});

// Field Projection

/*
    Inclusion

        SYNTAX:
            db.collectionName.find(
                {
                    criteria
                },

                {
                    field: 1
                }
            );

*/

db.inventory.find(
        {
            "publisher": "JS Publishing House"
        },

        {
            "name": 1,    // fields display under the field name
            "author": 1,  // fields display under the field author
            "price": 1    // fields display under the field price
        }
    );


/*
    Exclusion

        SYNTAX:
            db.collectionName.find(
                {
                    criteria
                },

                {
                    field: 0
                }
            );

*/

db.inventory.find(
        {
            "author": "John Thomas"
        },

        {   
            "price": 0, // field under the price will not display
            "stocks": 0 // field under the stocks will not display
        }
    );

/*
    Suppressing the ID field

      -  when using field projection, field inclusion and field exclusion may not be used at the same time, however EXLUDING the "_id (ObjectID)" is the only exception to this rule.

*/
db.inventory.find(
        {
            "price": {
                $lte:5000
            }
        },

        {
            "_id": 0, 
            "name": 1,
            "author": 1
        }
    );


// Evaluation Query Operators

/*

    $regex operator

        Syntax:
            db.collectionName.find(
                {
                    field: $regex: 'pattern', $options: '$optionValue'
                }
            );
*/

// Case Sensitive query
db.inventory.find(
        {
            "author": {
                $regex: 'M'
            }
        }
    );

// Not Case Sensitive query
db.inventory.find(
        {
            "author": {
                $regex: 'M',
                $options: '$i'
            }
        }
    );

